# twig

[twig/twig](https://packagist.org/packages/twig/twig) Template language

* [*Custom Twig Filters and Functions*
  ](https://sararellano.medium.com/custom-twig-filters-and-functions-7a25ef6cfd86)
  2021-09 Sararellano
* [*SEO-friendly breadcrumbs with metadata in Symfony’s Twig*
  ](https://medium.com/@dotcom.software/seo-friendly-breadcrumbs-in-symfonys-twig-4ebf84a2d14d)
  2020-08 Margaret Nawrocka

# Build on `twig`
* symfony/ux-twig-component
* symfony/ux-live-component
